#! /usr/local/bin/python

import argparse
from dataclasses import dataclass
import json
from pathlib import Path
import re

from typing import List, Dict, Optional, Union


@dataclass(eq=True, frozen=True)
class Annotation:
    label: str
    start: int
    end: int
    contents: str

    def span(self) -> tuple:
        return (self.start, self.end)

    def __len__(self) -> int:
        return self.end - self.start


def overlap(a, b):
    """Check if ranges overlap"""
    if set(range(*a.span())) & set(range(*b.span())):
        return True
    else:
        return False


def get_ann_file(fn: Union[Path, str]) -> Path:
    return Path(re.sub(r".txt", r".ann", str(fn)))


def get_existing_annotations(fn: Union[Path, str]) -> List[Annotation]:
    existing_anns: List[Annotation] = []

    with get_ann_file(fn).open("r") as f:
        contents = f.read()

    if not contents:
        return existing_anns

    for line in contents.split("\n"):
        try:
            _, lab, start, end, *pattern = line.strip().split()
        except ValueError:
            print(f"Error in line: '{line}'")
            raise
        existing_anns.append(Annotation(lab, int(start), int(end), " ".join(pattern)))

    return existing_anns


def annotate(
    contents: str,
    existing_annotations: Optional[List[Annotation]],
    patterns_d: Dict[str, str],
) -> List[str]:

    annotations_l = existing_annotations if existing_annotations else []

    for regex, lab in patterns_d.items():
        for m in re.finditer(rf"{regex}", contents):
            A = Annotation(lab, m.start(), m.end(), m.group())
            # If entities overlap, keep the longest and remove the rest.
            subs = [b for b in annotations_l if overlap(A, b)]
            if subs:
                for s in subs:
                    annotations_l.remove(s)
                subs.append(A)
                annotations_l.append(max(subs, key=lambda x: len(x)))
            else:
                annotations_l.append(A)
    return [
        f"T{idx+1}\t{a.label} {a.start:<3} {a.end:<3}\t{a.contents}"
        for idx, a in enumerate(sorted(set(annotations_l), key=lambda x: x.start))
    ]


def parse(
    contents: str, existing_patterns: Dict[str, str], labels: List[str]
) -> Dict[str, str]:
    """Parse patterns in annotation files. Returns dict(<pattern>: <label>)."""
    patterns = existing_patterns
    for line in contents.split("\n"):
        _, lab, _, _, *pattern = line.strip().split()
        patterns[" ".join(pattern)] = lab
    if labels:
        patterns = {k: v for k, v in patterns.items() if v in labels}
    return patterns


def run() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        dest="dir",
        type=str,
        nargs="?",
        default=Path.cwd(),
        metavar="dir",
        help="Target directory (default: working directory)",
    )
    parser.add_argument(
        "-f",
        dest="files",
        type=str,
        nargs="*",
        metavar="file",
        help="files to annotate or parse",
    )
    parser.add_argument(
        "-F",
        dest="pattern_file",
        type=str,
        nargs="?",
        default="annotations.json",
        metavar="file",
        help="specify json with label patterns, (default: 'annotations.json')",
    )
    parser.add_argument(
        "-a",
        dest="annotation",
        type=str,
        nargs=2,
        metavar=("PATTERN", "LABEL"),
        help="a single annotation",
    )
    parser.add_argument(
        "-l",
        dest="labels",
        type=str,
        nargs="+",
        metavar="label",
        default=[],
        help="specify labels to annotate",
    )
    parser.add_argument(
        "-w",
        dest="write",
        action="store_true",
        default=False,
        help="write contents (default: print to stdout)",
    )
    parser.add_argument(
        "-p", dest="parse", action="store_true", default=False, help="parse .ann files"
    )

    args = parser.parse_args()

    try:
        P = Path(args.dir)
    except TypeError:
        print(f"Invalid path: {args.dir}\n")
        return
    if not P.exists():
        print(f"Invalid path: {args.dir}\n")
        return

    if args.annotation:
        patterns_d = dict([args.annotation])
    else:
        pat_file = Path(args.pattern_file)
        if not pat_file.exists():
            patterns_d = {}
            with pat_file.open("w") as f:
                json.dump(patterns_d, f)
        else:
            with pat_file.open("r") as f:
                patterns_d = json.load(f)

    if not args.files:
        if args.parse:
            input = P.glob("*.ann")
        else:
            input = P.glob("*.txt")
    else:
        # Join specified filenames to the directory specified in 'args.dir'
        input = (Path(P, fn) for fn in args.files)

    for fn in input:
        if args.parse:
            try:
                with open(fn, "r") as f:
                    patterns = parse(f.read(), patterns_d, args.labels)
                with pat_file.open("w") as f:
                    json.dump(patterns, f)
            except json.JSONDecodeError as err:
                print(err.doc)
                raise
        else:
            if args.labels:
                patterns_d = {
                    k: v for k, v in patterns_d.items() if v in args.labels
                }

            existing_ann_file = get_ann_file(fn)
            with open(fn, "r") as f:
                anns = annotate(
                    f.read(),
                    get_existing_annotations(existing_ann_file),
                    patterns_d,
                )

            with open(existing_ann_file, "w") as f:
                f.write("\n".join(anns))


if __name__ == "__main__":
    run()
